;;; init.el  --- The Emacs Initialization File
;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;
;;; Commentary:
;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;
;;; Code:

(add-to-list 'load-path (expand-file-name "lisp" user-emacs-directory))

(require 'init-elpa)
(require 'init-exec-path)
(require 'init-ui)
(require 'init-editing)
(require 'init-navigation)
(require 'init-miscellaneous)
(require 'init-company-mode)
(require 'init-asciidoc)
(require 'init-lisp)
(require 'init-web-mode)
(require 'init-rust)
(require 'init-clojure)
(require 'init-cmake)
(require 'init-cpp)
(require 'init-scala)
(require 'init-haskell)
(require 'init-python)

(provide 'init)
;;; init.el ends here
